// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.


$(function() {

        setTimeout(updateGame, 10000);

});
function updateBoard(time){

    $('#boardContainer').attr("data-updated-at",time)
}
function updateGame(){

    var game_id =$('#boardContainer').attr("data-id")
    var game_updated_at =$('#boardContainer').attr("data-updated-at")
    $.getScript("/games/" + game_id + "?updated_at=" +game_updated_at );
    setTimeout(updateGame, 10000);
}
function updateInfo(score,current_turn){
    for (var i =0;i<score.length;i++){
        $('#score_'+score[i][0]).text(score[i][1]);
        $('#player_turn_'+score[i][0]).text(score[i][2])
    }
    $('#currentTurn').text(current_turn)
}
function activateDragAndDrop()
{
    $( ".draggable" ).draggable({ revert: "invalid" ,
        helper: "clone"});

    $( ".droppable" ).droppable({
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        accept: function(ui){
            if( $(this).children().size() > 0)
            {
                return false;
            } else {
                return true;
            }
        },
        drop: function( event, ui ) {
            var clone =ui.draggable.clone();
            clone.draggable({ revert: "invalid" ,
                helper: "clone"});
            $(this).append(clone)
            ui.draggable.parent();
            ui.draggable.remove();


        }
    });
}

function setHiddenMoveForm(){
   var s=$('.boardCell > .tile ').parent()
    var tiles =[];
    for(var i =1; i<= s.length;i++){

        $('#tile_'+i).val($(s[i-1]).find('span').attr("class"));
        $('#tile_'+i+'_x').val($(s[i-1]).attr('posx'));
        $('#tile_'+i+'_y').val($(s[i-1]).attr('posy'));
//        var tile={
//            'letter': $(s[i]).find('span').attr("class"),
//            'posx': $(s[i]).attr('posx'),
//            'posy': $(s[i]).attr('posy')
//        }
//        tiles.push(tile)
    }

}
function updateTable(board){
    for(var y =0; y< board.length;y++){
        var wea =0;
        for (var x = 0;x<board.length;x++){
            var cell = $('#cell_'+y+'_'+x);
            if (board[y][x][0]==null){
                cell.addClass("droppable")
                cell.addClass("ui-droppable")
                cell.empty()
            }
            else
            {
                cell.empty()
                cell.removeClass("droppable")
                cell.removeClass("ui-droppable")
                var tile = jQuery('<div/>', {
                    id: 'fixed_tile_'+y+'_'+x,
                    class: 'fixed_tile ui-widget-content'
                     });
                jQuery('<span/>',{
                    class: 'letter_'+board[y][x][0]
                }).appendTo(tile);
                jQuery('<span/>',{
                    class: 'score_'+board[y][x][1]
                }).appendTo(tile)
                tile.appendTo(cell)

            }
        }

    }
}

function updatePlayerTiles(tiles){
    activateDragAndDrop();
    for (var i = 0;i<tiles.length;i++) {
        var tilecase = $('#case'+(i+1));
        tilecase.empty();
        var tile = jQuery('<div/>', {
            id: 'tile1',
            class: 'tile draggable ui-widget-content ui-draggable'
        });
        jQuery('<span/>',{
            class: 'letter_'+tiles[i][0]
        }).appendTo(tile);
        jQuery('<span/>',{
            class: 'score_'+tiles[i][1]
        }).appendTo(tile)
        tile.appendTo(tilecase)
    }
    $('#play_form input.tile-input').each(function(index,element){$(element).val("")})

    activateDragAndDrop();
}
$(document).ready(function(){activateDragAndDrop()});
