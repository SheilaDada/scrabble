class GamesController < ApplicationController
  include GamesHelper
  # GET /games
  # GET /games.json
  def index
    @games = Game.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @games }
    end
  end

  # GET /games/1
  # GET /games/1.json
  def show
    @game = Game.find(params[:id])
    states_game = @game.states
    @game_status = []
    @states=@game.states
    states_game.each do |s|
      state_player = State.find(s)
      #games_status recibe la tripla nome jugador, turno y score
      @game_status.push([Player.find(state_player.player_id).name, state_player.turn, state_player.score,state_player.player_id])
    end

    @player = current_player
    @player_state = @player.states.where(:game_id => @game).last

    @next_turn = @game.next_turn
    @playable= @game.check_if_players_turn(@player)

    respond_to do |format|
      format.html # show.html.erb
      format.js do
        if params[:updated_at].to_i  < @game.updated_at.to_i
          render 'show'
        else
          render js: {}
        end
      end
      format.json { render json: @game }
    end
  end

  # GET /games/new
  # GET /games/new.json
  def new
    @game = Game.new
    @player = current_player

    @player = Player.new if !current_player



    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @game }
    end
  end

  # GET /games/1/edit
  def edit
    @game = Game.find(params[:id])
  end

  # POST /games
  # POST /games.json

  def create

    @game = Game.create()

    @player = current_player
    @player2 = Player.find_by_mail(params[:user1])
    #@player2 = Player.create(mail: params[:user1][:invited_email] )  if (!@player2 && !params[:user1][:invited_email].empty?)

    @player3 = Player.find_by_mail(params[:user2])
    #@player3 = Player.create(mail: params[:user2][:invited_email] ) if (!@player3 && !params[:user2][:invited_email].empty?)

    @player4 = Player.find_by_mail(params[:user3])
    #@player4 = Player.create(mail: params[:user3][:invited_email] )  if (!@player4 && !params[:user3][:invited_email].empty?)

    turn = 1
    if @player
      @state = @player.states.create(:game_id => @game, :turn => turn, :player_state => 'active')
      turn= turn+1
    end
    if @player2
      @state2 = @player2.states.create(:game_id => @game, :turn => turn, :player_state => 'active')
      turn= turn+1
    end
    if @player3 && @player3 != @player2
      @state3 = @player3.states.create(:game_id => @game, :turn => turn, :player_state => 'active')
      turn= turn+1
    end
    if @player4 && @player4 != @player2 && @player3 != @player4
      @state4 = @player4.states.create(:game_id => @game, :turn => turn, :player_state => 'active')
      turn= turn+1
    end

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render json: @game, status: :created, location: @game }
      else
        format.html { render action: "new" }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end



  # PUT /games/1
  # PUT /games/1.json
  def update

    @game = Game.find(params[:id])
    @player = current_player
    @player_state = @player.states.where(:game_id => @game).last
    @states = @game.states;
    success = @game.submit_moves params,@player
    #no supe como actualizarlo
    @game = Game.find(params[:id])
    respond_to do |format|
      if success
        format.html { redirect_to @game, notice: 'Accepted move.' }
        format.js
        format.json { head :no_content }
      else
        format.html { redirect_to @game, notice: 'Invalid move.' }
        format.js
      end
    end
  end
  def end

    @game = Game.find(params[:id])
    @player = current_player
    @player_state = @player.states.where(:game_id => @game).last

    success = @game.surrender @player

    #no supe como actualizarlo
    respond_to do |format|
      if success
        format.html { redirect_to @player, notice: 'Juego Terminado' }
        format.js
        format.json { head :no_content }
      else
        format.html { redirect_to @game, notice: 'Invalid move.' }
        format.js
      end
    end
  end
  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game = Game.find(params[:id])
    @game.destroy

    respond_to do |format|
      format.html { redirect_to games_url }
      format.json { head :no_content }
    end
  end
end
