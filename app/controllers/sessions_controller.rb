class SessionsController < ApplicationController


  def new
    if current_player
      redirect_to player_path (current_player)
    end  
  end

  def create
    player = Player.authenticate(params[:mail], params[:password])
    if player
      session[:player_id] = player.id
      redirect_to player_path (player), :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
    session[:player_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end
