class PlayersController < ApplicationController
  # GET /players
  # GET /players.json
  def index
    @players = Player.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @players }
    end
  end

  # GET /players/1
  # GET /players/1.json
  def show
    @player = Player.find(params[:id])
    
    #player_states = @player.states
    #@games_player = []

    #player_states.each do |s|
     # state_player = State.find(s)
      #games_player recibe los ids de los games que el jugador participa y está activo
      #if state_player.player_state = "active"
       # names_player = []
        #game_active = Game.find(state_player.game_id)
        # quiero encontrar los jugadores que participan del juego
        #game_active.states.each do |h|
         # player_participant = State.find(h)
          #player_participant2 = Player.find(player_participant.player_id)
          #names_player.push(player_participant2.name)
        #end  
       #@games_player.push([state_player.game_id, game_active.created_at, names_player])
      #end 
    #end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @player }
    end
  end

  # GET /players/new
  # GET /players/new.json
  def new
    
    if current_player
      redirect_to player_path (current_player)
    else
      @player = Player.new
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @player }
      end
    end
  end

  # GET /players/1/edit
  def edit
    @player = Player.find(params[:id])
  end

  # POST /players
  # POST /players.json
  def create
    @player = Player.new(params[:player])

    respond_to do |format|
      if @player.save
          session[:player_id] = @player.id
        format.html { redirect_to @player, notice: 'Player was successfully created.' }
        format.json { render json: @player, status: :created, location: @player }
      else
        format.html { render action: "new" }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /players/1
  # PUT /players/1.json
  def update
    @player = Player.find(params[:id])

    respond_to do |format|
      if @player.update_attributes(params[:player])
        format.html { redirect_to @player, notice: 'Player was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    @player = Player.find(params[:id])
    reset_session
    @player.destroy
    respond_to do |format|
      format.html { redirect_to root_url }
      format.json { head :no_content }
    end
  end

  def ranking
    @players = Player.all.sort_by {|player| player.max_score}.reverse
    respond_to do |format|
      format.html
      format.json
    end
  end  
end
