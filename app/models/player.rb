class Player < ActiveRecord::Base

  attr_accessible :mail, :name, :password
  
  attr_accessor :password

  before_save :encrypt_password

  validates_presence_of :password, :on => :create
  validates_presence_of :mail
  validates_presence_of :name
  validates_uniqueness_of :mail

  has_many :states
  has_many :games, :through => :states
  has_many :moves

  def self.authenticate(mail, password)
    player = find_by_mail(mail)
    if player && player.password_hash == BCrypt::Engine.hash_secret(password, player.password_salt)
      player
    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def count_moves(game_id)
    count = self.moves.where(:game_id => game_id).maximum(:move_number)
    count == nil ?  0 :  count
  end

  def max_score
    if self.states.size > 0
      self.states.maximum("score")
    else
      0
    end
  end
end
