class Game < ActiveRecord::Base
  has_many :states
  has_many :players, :through => :states
  has_many :moves , :order=> 'move_number DESC'
  attr_accessible :language_id, :buildBoard, :score

  include GamesHelper

  def build_board
    m = Array.new(15) {Array.new(15) {[nil, false, false,0]}}
    moves = self.moves
    moves.each do |x|
      pos_x = x.coordinate.split(",")[0].to_i
      pos_y = x.coordinate.split(",")[1].to_i
      m[pos_x][pos_y][0] = x.letter
      m[pos_x][pos_y][3] = points_for_letter(x.letter)
    end
    return m
  end
  def build_board_for_ajax
    m = Array.new(15) {Array.new(15) {[nil,0]}}
    moves = self.moves
    moves.each do |x|
      pos_x = x.coordinate.split(",")[0].to_i
      pos_y = x.coordinate.split(",")[1].to_i
      m[pos_x][pos_y][0] = x.letter
      m[pos_x][pos_y][1] = points_for_letter(x.letter)
    end
    m
  end
  def next_turn
     last_move = self.moves.first
     return 1 if last_move.nil?

     last_player_turn = last_move.player.states.where(game_id: self).first.turn


     (last_player_turn.to_i()) % (self.states.count) +1
  end
  def check_if_players_turn(player)

    last_move = self.moves.first
    if last_move
      last_player = last_move.player
      last_player_turn = last_player.states.where(:game_id => self).first.turn if last_player
    end


    states = State.where(:game_id => self)

    next_turn = (last_player_turn.to_i()) % (states.count) +1

    player_state = player.states.where(:game_id => self).last

    player_state.turn == next_turn
  end
  def generate_moves(params)
    @last_move = Move.where(:game_id =>self).maximum('move_number')
    moves=[]
    unless params[:tile_1].empty?
      m= Move.new()
      m.coordinate="#{params[:tile_1_x]},#{params[:tile_1_y]}"
      m.letter = params[:tile_1][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    unless params[:tile_2].empty?
      m= Move.new()
      m.coordinate="#{params[:tile_2_x]},#{params[:tile_2_y]}"
      m.letter = params[:tile_2][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    unless params[:tile_3].empty?
      m= Move.new()
      m.coordinate="#{params[:tile_3_x]},#{params[:tile_3_y]}"
      m.letter = params[:tile_3][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    unless params[:tile_4].empty?
      m= Move.new()
      m.coordinate="#{params[:tile_4_x]},#{params[:tile_4_y]}"
      m.letter = params[:tile_4][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    unless params[:tile_5].empty?
      m= Move.new()
      m.coordinate="#{params[:tile_5_x]},#{params[:tile_5_y]}"
      m.letter = params[:tile_5][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    unless params[:tile_6].empty?
      m= Move.new()
      m.coordinate="#{params[:tile_6_x]},#{params[:tile_6_y]}"
      m.letter = params[:tile_6][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    unless params[:tile_7].empty?
      m= Move.new()
      m.player_id = @player.id;
      m.coordinate="#{params[:tile_7_x]},#{params[:tile_7_y]}"
      m.letter = params[:tile_7][-1, 1]
      @last_move.nil? ? m.move_number = 1 : m.move_number = @last_move.to_int + 1
      moves.push(m)
    end
    moves
  end
  def submit_moves(params, player)

    return false unless self.check_if_players_turn player

    player_state = player.states.where(:game_id => self).last
    moves = self.generate_moves(params)
    m = self.build_board()
    ok_move = false
    words_created=nil


    if move_accepted(m, moves)
      words_created = validate_words(m)
      ok_move = save_move (words_created)
    end

    return false unless ok_move

    total_new_score = 0
    words_created.each do |word|
      total_new_score += word[1]
    end
    state = player_state
    play_score = total_new_score - self.score
    state.score += play_score
    state.replace_used_letters(moves)
    state.save!
    self.update_attributes(score: total_new_score)
    #self.score = total_new_score
    moves.each do |s|
      self.moves<<s
      player.moves<<s
    end
    self.touch
    player.save

  end
  def surrender (player)

    state = player.states.where(:game_id => self).last
    state.update_attributes(player_state:'inactive')
  end

end

