class State < ActiveRecord::Base
  attr_accessible :game_id, :user_id, :user_state, :turn, :player_state
  belongs_to :player
  belongs_to :game
  before_create :init

  include GamesHelper

  def init
     self.random_letters()
  end
  def random_letters()
    self.l1 = State.letter_distribution
    self.l2 = State.letter_distribution
    self.l3 = State.letter_distribution
    self.l4 = State.letter_distribution
    self.l5 = State.letter_distribution
    self.l6 = State.letter_distribution
    self.l7 = State.letter_distribution
  end
  def random_letter(i)
    if i==1
      self.l1 = State.letter_distribution
    elsif i==2
      self.l2 = State.letter_distribution
    elsif i==3
      self.l3 = State.letter_distribution
    elsif i==4
      self.l4 = State.letter_distribution
    elsif i==5
      self.l5 = State.letter_distribution
    elsif i==6
      self.l6 = State.letter_distribution
    elsif i==7
      self.l7 = State.letter_distribution
    end
  end
  def self.letter_distribution
    i = rand(1..100)
    if i<=9
      1
    elsif i<=11
      2
    elsif i<=13
      3
    elsif i<=17
      4
    elsif i<=29
      5
    elsif i<=31
      6
    elsif i<=34
      7
    elsif i<=36
      8
    elsif i<=45
      9
    elsif i<=46
      10
    elsif i<=47
      11
    elsif i<=51
      12
    elsif i<=53
      13
    elsif i<=59
      14
    elsif i<=67
      15
    elsif i<=69
      16
    elsif i<=70
      17
    elsif i<=76
      18
    elsif i<=80
      19
    elsif i<=88
      20
    elsif i<=92
      21
    elsif i<=94
      22
    elsif i<=96
      23
    elsif i<=97
      24
    elsif i<=99
      25
    else
      26
    end
  end
  def get_state_letter(i)
    if (i==1)
      return self.l1
    elsif(i==2)
      return self.l2
    elsif(i==3)
      return self.l3
    elsif(i==4)
      return self.l4
    elsif(i==5)
      return self.l5
    elsif(i==6)
      return self.l6
    else
      return self.l7
    end
  end
  def get_state_letters
    [[get_letter_from_number(self.l1.to_i),points_for_letter(get_letter_from_number(self.l1.to_i))],
     [get_letter_from_number(self.l2.to_i),points_for_letter(get_letter_from_number(self.l2.to_i))],
     [get_letter_from_number(self.l3.to_i),points_for_letter(get_letter_from_number(self.l3.to_i))],
     [get_letter_from_number(self.l4.to_i),points_for_letter(get_letter_from_number(self.l4.to_i))],
     [get_letter_from_number(self.l5.to_i),points_for_letter(get_letter_from_number(self.l5.to_i))],
     [get_letter_from_number(self.l6.to_i),points_for_letter(get_letter_from_number(self.l6.to_i))],
     [get_letter_from_number(self.l7.to_i),points_for_letter(get_letter_from_number(self.l7.to_i))]]
  end
  def replace_used_letters(moves)
    notUsed = Array.new(7) { true}
    moves.each do |move|
      if move.letter==get_letter_from_number(self.l1.to_i)  && notUsed[0]
        notUsed[0]= false
        self.random_letter(1)
      elsif move.letter==get_letter_from_number(self.l2.to_i) && notUsed[1]
        self.random_letter(2)
        notUsed[1]= false
      elsif move.letter==get_letter_from_number(self.l3.to_i) && notUsed[2]
        self.random_letter(3)
        notUsed[2]= false
      elsif move.letter==get_letter_from_number(self.l4.to_i) && notUsed[3]
        self.random_letter(4)
        notUsed[3]= false
      elsif move.letter==get_letter_from_number(self.l5.to_i) && notUsed[4]
        self.random_letter(5)
        notUsed[4]= false
      elsif move.letter==get_letter_from_number(self.l6.to_i) && notUsed[5]
        self.random_letter(6)
        notUsed[5]= false
      elsif move.letter==get_letter_from_number(self.l7.to_i) && notUsed[6]
        self.random_letter(7)
        notUsed[6]= false
      end
    end

  end
end
