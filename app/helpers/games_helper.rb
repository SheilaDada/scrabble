module GamesHelper

  require 'net/http'
  #diccionario

  $words = {}

  File.open("public/wordsEn.txt", "r") do |file|
    file.each do |line|
      $words[line.strip] = true
    end
  end 

	  #Haciendo el calculo del puntaje del juego, todos retornan verdaderos si pertenecen al puntaje
  def double_letter(y,x)
  	coordinate = [[0,3],[0,11],[2,6],[2,8],[3,0],[3,7],[3,14],[6,2],[6,6],[6,8],[6,12],[7,3],[7,11],[8,2],[8,6],[8,8],[8,12],[11,0],[11,7],[11,14],[12,6],[12,8],[14,3],[14,11]]
  	result = false
  	coordinate.each do |i,j|	
  		if i == y and j == x
  			result = true
  			break
  		else
  			result = false
  		end
  	end
  	result
  end

  def triple_word(y,x)
  	if [0,7,14].include? y and [0,7,14].include? x and !(x == y and x == 7)
  		true
  	else
  		false
  	end
  end

  def double_word(y,x)
  	coordinate = [[1,1],[2,2],[3,3],[4,4],[10,10],[11,11],[12,12],[13,13],[1,13],[2,12],[3,11],[4,10],[10,4],[11,3],[12,2],[13,1]]
  	result = false
  	coordinate.each do |i,j|	
  		if i == y and j == x
  			result = true
  			break
  		else
  			result = false
  		end
  	end
  	return result
  end 

  def triple_letter(y,x)
  	coordinate = [[1,5],[1,9],[5,1],[5,5],[5,9],[5,13],[9,1],[9,5],[9,9],[9,13],[13,5],[13,9]]
  	result = false
  	coordinate.each do |i,j|	
  		if i == y and j == x
  			result = true
  			break
  		else
  			result = false
  		end
  	end
  	return result
  end
  

  def get_letter_from_number(n)
    n=n+64
    n.chr
  end

  def points_for_letter(letter)
      l = letter.upcase.to_sym
      point_hash = {A: 1,
                   B: 3,
                   C: 3,
                   D: 2,
                   E: 1,
                   F: 4,
                   G: 2,
                   H: 4,
                   I: 1,
                   J: 8,
                   K: 5,
                   L: 1,
                   M: 3,
                   N: 1,
                   O: 1,
                   P: 3,
                   Q: 10,
                   R: 1,
                   S: 1,
                   T: 1,
                   U: 1,
                   V: 4,
                   W: 4,
                   X: 8,
                   Y: 4,
                   Z: 10}

      point_hash[l]
  end

  #esta función compara el diccionario con las palabras creadas
  def save_move(words_created)
    if words_created.size == 0
      return false
    end
    words_created.each do |word_c|
      word = word_c[0].downcase
      # original, sin la api
      #unless $words[word]
        #return false
      url = URI.parse('http://api.wordreference.com/1672a/json/enfr/' + word)
      req = Net::HTTP::Get.new(url.path)
      res = Net::HTTP.start(url.host, url.port){|http|
      http.request(req)
      }
      parsed_json = ActiveSupport::JSON.decode(res.body)
      if !parsed_json ["Error"].nil?
        return false
      end      
    end  
     true
  end

  #words_created tiene la palabra creada y el puntaje de esa palabra
  def validate_words(m)
    words_created = []
    15.times do |i|
      15.times do |j|
        if !m[i][j][0].nil?
          if !m[i][j][1]
            word_right = right_word(m,i,j)
            if word_right[0].length > 1
              words_created.push(word_right)
            end  
          end  
          if !m[i][j][2]
            word_down = down_word(m,i,j)  
            if word_down[0].length > 1
              words_created.push(word_down)
            end  
          end  
        end 
      end 
    end
    return words_created   
  end

  def right_word(m,i,j)
    right = ""
    double_w = false
    triple_w = false
    score_word = 0
    j.upto(14) do |k|
      if !m[i][k][0].nil?
        #puts "4444444444444444444444444444444444444444444444444444444444444444444"
        #puts m[i][k][0]
        right << m[i][k][0]
        m[i][k][1] = true

        multiplier = 1
        if(get_cell_class(i,k) == "double_letter")
          multiplier = 2
        elsif (get_cell_class(i,k) == "triple_letter")
          multiplier = 3
        end
        score_word += multiplier * points_for_letter(m[i][k][0])
              
        if(get_cell_class(i,k) == "triple_word")
          triple_w = true
        elsif (get_cell_class(i,k) == "double_word")
          double_w = true
        end

      else
        if triple_w
          score_word *= 3
        elsif double_w
          score_word *= 2
        end
        return [right, score_word]
      end    
    end
    if triple_w
      score_word *= 3
    elsif double_w
      score_word *= 2
    end
    return [right, score_word]
  end
  
  def down_word(m,i,j)
    down = ""
    double_w = false
    triple_w = false
    score_word = 0
    i.upto(14) do |k|
      if !m[k][j][0].nil?
        #puts "4444444444444444444444444444444444444444444444444444444444444444444"
        #puts m[i][k][0]
        down << m[k][j][0]
        m[k][j][2] = true

        multiplier = 1
        if(get_cell_class(k,j) == "double_letter")
          multiplier = 2
        elsif (get_cell_class(k,j) == "triple_letter")
          multiplier = 3
        end
        score_word += multiplier * points_for_letter(m[k][j][0])
              
        if(get_cell_class(k,j) == "triple_word")
          triple_w = true
        elsif (get_cell_class(k,j) == "double_word")
          double_w = true
        end

      else
        if triple_w
          score_word *= 3
        elsif double_w
          score_word *= 2
        end
        return [down, score_word]
      end
    end
    if triple_w
      score_word *= 3
    elsif double_w
      score_word *= 2
    end
    return [down, score_word]
  end

  def get_cell_class(i,j)

    if triple_word(i,j)
      return 'triple_word'
    elsif double_word(i,j)
      return 'double_word'

    elsif double_letter(i,j)
      return 'double_letter'
    elsif triple_letter(i,j)
      return 'triple_letter'
    elsif i == j and i ==7
      return 'center_cell'
    else
      return 'normal_cell'
    end

  end

  # no se puede poner letras en horizontales Y verticales distintas, sólo una variable puede ser distinta
  #validación de todas las letras en horizontal o vertical
  def move_accepted(m, moves)

    return false if moves.empty?
    x_first = moves.first.coordinate.split(",")[0].to_i
    y_first = moves.first.coordinate.split(",")[1].to_i
    horiz = false
    vertical = false

    ##Aqui falta el IF para que el mapa comience en el centro;
    #if m[7][7].nil?

    #iterar sobre moves para ver si alguno esta en el medio

    #si ya existe la primera jugada, habria que verificar que alguna letra de moves al lado de una letra del mapa
    #else

    # iterar sobre cada moves y revisar sus casillas cercanas
    # ojo que si es un borde y se accede como m[x_first + 1 ][y_first][0] el programa se cae

    #end


    if moves.size == 1
      #verifico si esta en algun borde antes de comprobar si hay alguna letra al lado
      #saque esta parte pq se tiene que hacer mas arriba

      #if m[x_first + 1].nil? || m[x_first -1].nil? || m[x_first][y_first + 1].nil? || m[x_first][y_first -1].nil?
      #  return false
      #end
      #
      #if m[x_first + 1 ][y_first][0] == nil and m[x_first -1 ][y_first][0] == nil and m[x_first][y_first + 1][0] == nil and m[x_first][y_first -1][0] == nil
      #  return false
      moves.each do |x|
        pos_x = x.coordinate.split(",")[0].to_i
        pos_y = x.coordinate.split(",")[1].to_i
        m[pos_x][pos_y][0] = x.letter
      end
      return true
    else  
      moves.each do |x|
        pos_x = x.coordinate.split(",")[0].to_i
        if pos_x != x_first
          horiz = true
        end  
        pos_y = x.coordinate.split(",")[1].to_i
        if pos_y != y_first
          vertical = true
        end
        
        if vertical and horiz
           return false
        else
          m[pos_x][pos_y][0] = x.letter
        end
      end
    end
    true
  end

end