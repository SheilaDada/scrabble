class RemovePasswordFromPlayers < ActiveRecord::Migration
  def up
    remove_column :players, :password
  end

  def down
    add_column :players, :password, :string
  end
end
