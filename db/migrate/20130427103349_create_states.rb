class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.integer :player_id
      t.string :game_id
      t.string :player_state
      t.string :l1
      t.string :l2
      t.string :l3
      t.string :l4
      t.string :l5
      t.string :l6
      t.string :l7
      t.timestamps
    end
  end
end

