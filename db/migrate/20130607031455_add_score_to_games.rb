class AddScoreToGames < ActiveRecord::Migration
  def change
  	add_column :games, :score, :integer, :default => 0
  	add_column :states, :score, :integer, :default => 0
  end
end
