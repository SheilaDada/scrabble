class CreateMoves < ActiveRecord::Migration
  def change
    create_table :moves do |t|
      t.string :coordinate
      t.integer :player_id
      t.string :letter
      t.integer :move_number
      t.integer :game_id

      t.timestamps
    end
  end
end
