class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :language_id

      t.timestamps
    end
  end
end
